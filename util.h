#pragma once

#include <string>
#include <SFML/Graphics.hpp>

#define TEX_SIZE		56
#define TEX_CENTER		28
#define CHESS_MAN_NUM	32
#define WINDOW_W		453
#define WINDOW_H		453
#define BOARD_SIZE		8
#define FPS_MAX			60


sf::Vector2i toCoord(char a, char b);
std::string toChessNote(sf::Vector2f p);

