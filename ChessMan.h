#pragma once

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>


enum TYPEMAN { KING, QUEEN, ROOK, BISHOP, KNIGHT, PAWN, OUT };
enum SIDE { WHITE, BLACK, UNSET };

struct ChessManInfo
{
	int xLocation;
	int yLocation;
	int SIDE;
	TYPEMAN type;
	sf::Sprite Sprite;
	ChessManInfo(int x, int y) {
		xLocation = x;
		xLocation = y;
	}
};

class ChessMan
{
public:

	/*
	*/
	ChessMan(ChessManInfo);

	~ChessMan() {};

	void SetLocation(int,int);
	void SetSideAndType(int, TYPEMAN);


	ChessManInfo& GetInfor() ;

	bool Configuration();

	void Draw(
		std::shared_ptr<sf::RenderWindow>&) ;

	void PrintInformation();

private:

	sf::Texture sTexture;
	std::shared_ptr<ChessManInfo> mInfo;
};










