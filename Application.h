#pragma once
#include <string>
#include <memory>
#include <SFML/Graphics.hpp>

#include "Board.h"

class Application 
{
public:

    Application() = default;
    void InitWindow( std::string );
    void Looping( );
    void TerminateWindow( );

private:

    /*
    * window attributes
    * with setting and title
    */
    std::string mTitle;
    std::shared_ptr<sf::RenderWindow> mWindow;
    std::shared_ptr<sf::Event> mEvent;
    std::unique_ptr<CBoard> mBoard;
};

