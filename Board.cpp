#include "Board.h"
#include <SFML/Graphics.hpp>
#include <algorithm>
#include <iostream>



void Board::LoadChessmans()
{

    for (int i = 0; i < BOARD_SIZE; ++i)
    {
        for (int j = 0; j < BOARD_SIZE; ++j)
        {
            int n = board[i][j];
            int side = (n > 0) ? 1 : 0;
            if (!n) continue;
            int x = abs(n) - 1;
            int y = n > 0 ? 1 : 0;
                //= new Chessman::ChessMan(x, y, i, j, abs(n), side);
            std::shared_ptr<ChessManInfo> ptr( new ChessManInfo());
            auto ptr = std::make_shared<ChessManInfo> ;

            if (ptr != NULL) vChessmans.push_back(ptr);

        }
    }
}

void Board::Setting(
    std::shared_ptr<sf::RenderWindow>& w,
    std::shared_ptr<sf::Event>& e)
{
    /* setting sprite */
    sTexture.loadFromFile("images/board0.png");
    sBoard.setTexture(sTexture);

    /* Create chessmans */ 
    LoadChessmans();

    /* holding window intance */
    mWindow = w;

    /* sharing event intance */
    mEvent = e;
}

Board::~Board() {
    /* release memory */
    for (auto chessman : vChessmans)
        if (chessman != NULL) delete chessman;

}


void Board::HandleEvent()
{

    while (mWindow->pollEvent(*mEvent.get()))
    {
        /* Close window */
        if (mEvent->type == sf::Event::Closed)

            mWindow->close();

        /* move back */
        if (mEvent->type == sf::Event::KeyPressed)
        {
            
            if (mEvent->key.code == sf::Keyboard::BackSpace) {
                MoveBack();
            }
        }

        /* drag and drop */
        if (mEvent->type == sf::Event::MouseButtonPressed) {
            
            if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {

                std::cout << "Left mouse pressed" << "\n";

                DragAndDrop();
            }
        }
        if (mEvent->type == sf::Event::MouseButtonReleased) {
            if (mEvent->key.code == sf::Mouse::Left) {

                std::cout << "Mouse button released" << "\n";

                ReleaseButton();
            }
        }


    }
}

void Board::DragAndDrop()
{
    sf::Vector2i OFFSET(TEX_CENTER, TEX_CENTER);
    mPos = sf::Mouse::getPosition() - sf::Vector2i(OFFSET);

    std::cout << "mouse x: " << mPos.x << " - mouse y:" << mPos.y << "\n";
    std::vector <Chessman::ChessMan*>::iterator it;
    //for (it = vChessmans.begin(); it != vChessmans.end(); ++it) {
    //    if ((*it)->GetInfor().Sprite.getGlobalBounds().contains(mPos.x, mPos.y)) {
    //        mIsMove = true;
    //        mDx = mPos.x - (*it)->GetInfor().xLocation;
    //        mDy = mPos.y - (*it)->GetInfor().yLocation;
    //    }

    //    iptr = it;
    //}

    
    it = vChessmans.begin();
    Chessman::ChessManInfo i = (*it)->GetInfor();
    std::cout << "e1 x: " << i.xLocation << " - e1 y:" << i.yLocation << "\n";
    (*it)->GetInfor().Sprite.setPosition(mPos.x, mPos.y);


}

void Board::ReleaseButton()
{
    mIsMove = false;
}

void Board::MoveBack()
{

}

void Board::UpdateState() {

    if (mIsMove) 
        (*iptr)->GetInfor()
        .Sprite
        .setPosition(mPos.x - mDx, mPos.y - mDy);
}
void Board::UpdatePosition(int x, int y) {

}


void Board::HandleRender()
{
    mWindow->clear();

    Draw();

    mWindow->display();
}

void Board::Draw()
{
    /* draw board game */
    mWindow->draw(sBoard);

    /* draw chessmans */
    for (const auto& chessMan : vChessmans) {

        chessMan->Draw(mWindow);
    }
}
