#include "Application.h"
#include "util.h"
#include <iostream>

/*
* 
*/


void Application::InitWindow(std::string title ){

	/* create display main window */
	mWindow.reset(new sf::RenderWindow(
		sf::VideoMode(WINDOW_W, WINDOW_H),
		title.c_str() ) );

	mWindow->setFramerateLimit(FPS_MAX);

	/* create event handler */
	mEvent.reset(new sf::Event());

	/* create board and configuration */ 
	mBoard.reset(new CBoard());
	mBoard->Setting(mWindow, mEvent);

}

void Application::Looping()
{

	while (mWindow->isOpen())
	{
		mBoard->HandleEvent();

		//mBoard->UpdateState();
		//mBoard->UpdatePosition();

		mBoard->HandleRender();
	}
}

void Application::TerminateWindow()
{

}
