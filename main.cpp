#include "Application.h"
#include <iostream>


int main()
{
    std::string title("ChessBoard!");
    try {
        Application app = Application();

        app.InitWindow(title);

        app.Looping();
    }
    catch (const std::exception& e) {
        std::cout << e.what() << '\n';
    }
    return 0;
}
