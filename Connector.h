#pragma once

#include <string>

class Connector
{
public:
	virtual void ConnectToEngine( const std::string& ) const = 0;
	virtual std::string GetResponseFromEngine( std::string& ) const = 0;
	virtual void CloseConnection() const = 0;
};

