#pragma once

#include <SFML/Window.hpp>
#include <vector>
#include <memory>
#include "util.h"
#include "ChessMan.h"

/*
  ASCII a char start at decimal 97 and 1 at decimal 49
	 a  b  c  d  e  f  g  h
  8                         8
  7                         7
  6                         6
  5                         5
  4                         4
  3                         3
  2                         2
  1                         1
	 a  b  c  d  e  f  g  h
*/

/*
  ASCII a char start at decimal 97 and 1 at decimal 49
  1 2 3 4 5 6 7 8
  1 2 3 4 5 6 7 8
*/



static int board[BOARD_SIZE][BOARD_SIZE] = {
	-1,-2,-3,-4,-5,-3,-2,-1,
	-6,-6,-6,-6,-6,-6,-6,-6,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0,
	6, 6, 6, 6, 6, 6, 6, 6,
	1, 2, 3, 4, 5, 3, 2, 1
};


class Board
{
public:

	void Setting(
		std::shared_ptr<sf::RenderWindow>&,
		std::shared_ptr<sf::Event>&);

	/* Operation functions  */
	void DragAndDrop();
	void MoveBack();
	void LoadChessmans();
	void ReleaseButton();
	void UpdateState();
	void UpdatePosition(int,int);
	/* Application handle functions  */
	void HandleEvent();
	void HandleRender();


	void Draw( ) ;

	~Board();

private:

	/*
	* chess board attributes with actions
	* Board handle chess mand motion
	*/
	int		mBoard[TEX_SIZE][TEX_SIZE];
	bool	mIsMove;
	float	mDx, mDy;
	sf::Vector2i mPos;
	sf::Vector2f mOldPosition;
	sf::Vector2f mNewPosition;
	sf::Sprite sBoard;
	sf::Texture sTexture;
	std::vector <Chessman::ChessMan*> vChessmans;
	//std::unique_ptr<Chessman::ChessMan*> vChessmans;
	std::shared_ptr<sf::RenderWindow> mWindow;
	std::shared_ptr<sf::Event> mEvent;

	std::vector <Chessman::ChessMan*>::iterator iptr;
};



