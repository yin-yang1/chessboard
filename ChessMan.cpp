#include "ChessMan.h"
#include "util.h"
#include <iostream>

/*
* @Texture x
* @Texture y
* @BoardLocation i
* @BoardLocation j
* @Type
* @Side
*/
ChessMan::ChessMan(ChessManInfo i)
{
	/* Create default texture */
	sTexture.loadFromFile("images/figures.png");

	/* Setup chessman information */
	mInfo = i;

	/* configuration Texture */
	mInfo.Sprite.setTexture(sTexture);
	mInfo.Sprite.setTextureRect(
		sf::IntRect(
			TEX_SIZE * x,
			TEX_SIZE * y, 
			TEX_SIZE, 
			TEX_SIZE
		)
	);

	// first set location 
	mInfo.Sprite.setPosition(
		TEX_SIZE * mInfo.xLocation, 
		TEX_SIZE * mInfo.yLocation);

}

void ChessMan::SetLocation(int x, int y)  {

	mInfo.xLocation = x;
	mInfo.yLocation = y;
}

void ChessMan::SetSideAndType(int s, TYPEMAN tm) 
{
	mInfo.SIDE = s;
	mInfo.type = tm;
}

ChessManInfo& ChessMan::GetInfor()  {

	return mInfo;
}

bool ChessMan
::Configuration() {
	return false;
}

void ChessMan::PrintInformation()
{
	std::cout << "xLocation: " 
		<< mInfo.xLocation << " - " 
		<< "yLocation: " 
		<< mInfo.yLocation << '\n';
}


void ChessMan::Draw(
	std::shared_ptr<sf::RenderWindow>& w) 
{
	w->draw(mInfo.Sprite);
}